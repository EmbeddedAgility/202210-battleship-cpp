#include "Program.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

#include "../Battleship.GameController.Lib/GameController.h"

#pragma comment(lib,"winmm.lib")  //for MSV C++   

using namespace Battleship::GameController;
using namespace Battleship::GameController::Contracts;


// Global variable used for storing field size
int fieldSize = 8;

namespace Battleship
{
    
  namespace Ascii
  {
    ostream& operator<<(ostream &out, Position pos)
    {
      out << (char)('A' + pos.Column) << pos.Row;
      return out;
    }

    Program::Program()
    {
    }

    Program::~Program()
    {
    }



    list<Ship> Program::myFleet;
    list<Ship> Program::enemyFleet;
    
    void Program::Main()
    {
      cout << "\033[31m                                     |__                                       " << endl;
      cout << "\033[31m                                     | \ /                                     " << endl;
      cout << "\033[31m                                     ---                                       " << endl;
      cout << "\033[31m                                     / | [                                     " << endl;
      cout << "\033[31m                              !      | |||                                     " << endl;
      cout << "\033[31m                            _/|     _/|-++'                                    " << endl;
      cout << "\033[31m                        +  +--|    |--|--|_ |-                                 " << endl;
      cout << "\033[31m                     { /|__|  |/\__|  |--- |||__/                              " << endl;
      cout << "\033[31m                    +---------------___[}-_===_.'____                 /\       " << endl;
      cout << "\033[31m                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/    _  " << endl;
      cout << "\033[31m __..._____--==/___]_|__|_____________________________[___\==--____,------' .7 " << endl;
      cout << "\033[31m|                        Welcome to Battleship 2.0                        BB-61/  " << endl;
      cout << "\033[31m \_________________________________________________________________________|   " << endl;
      cout << endl;
      cout << "\033[0m";

	  InitializeGame();

      StartGame();
    }

    void Program::StartGame()
    {
      //Console::Clear();
      cout << "\033[31m                  __     " << endl;
      cout << "\033[31m                 /  \    " << endl;
      cout << "\033[31m           .-.  |    |   " << endl;
      cout << "\033[31m   *    _.-'  \  \__/    " << endl;
      cout << "\033[31m    \.-'       \         " << endl;
      cout << "\033[31m   /          _/         " << endl;
      cout << "\033[31m  |      _  /""          " << endl;
      cout << "\033[31m  |     /_\'             " << endl;
      cout << "\033[31m   \    \_/              " << endl;
      cout << "\033[31m    """"""""             " << endl;
      cout << endl;
      cout << "\033[0m";

      do
      {
        cout << endl;
        cout << R"(Player, it's your turn   )" << endl;



        bool PositionValid = false;
        string input;
        Position position;
        bool shotInField = false;
        bool isHit = false;


        while (shotInField == false) {
          cout << R"(Enter coordinates for your shot :   )" << endl;
          getline(cin, input);

          position = ParsePosition(input);


          //Check if in field range
          shotInField = checkInField(position);

          isHit = GameController::GameController::CheckIsHit(enemyFleet, position);
          if (isHit)
          {
              // Console::Beep();

            cout << "\033[32m                \         .  ./         " << endl;
            cout << "\033[32m              \      .:"";'.:..""   /   " << endl;
            cout << "\033[32m                  (M^^.^~~:.'"").       " << endl;
            cout << "\033[32m            -   (/  .    . . \ \)  -    " << endl;
            cout << "\033[32m               ((| :. ~ ^  :. .|))      " << endl;
            cout << "\033[32m            -   (\- |  \ /  |  /)  -    " << endl;
            cout << "\033[32m                 -\  \     /  /-        " << endl;
            cout << "\033[32m                   \  \   /  /          " << endl;
            cout << "\033[32mYeah ! Nice hit !" << "\033[0m" << endl;

          }
          else
          {
            cout << "\033[104mMiss" << "\033[0m" << endl;
          }
        }


        position = GetRandomPosition();
        isHit = GameController::GameController::CheckIsHit(myFleet, position);
        cout << endl;

        if (isHit)
        {
          //Console::Beep();

          cout << "\033[31m                \         .  ./         " << endl;
          cout << "\033[31m              \      .:"";'.:..""   /   " << endl;
          cout << "\033[31m                  (M^^.^~~:.'"").       " << endl;
          cout << "\033[31m            -   (/  .    . . \ \)  -    " << endl;
          cout << "\033[31m               ((| :. ~ ^  :. .|))      " << endl;
          cout << "\033[31m            -   (\- |  \ /  |  /)  -    " << endl;
          cout << "\033[31m                 -\  \     /  /-        " << endl;
          cout << "\033[31m                   \  \   /  /          " << endl;

          cout << "\033[31mComputer shoot in " << position << "\033[31m and " << "\033[31mhit your ship !" << "\033[0m" << endl;

        }
        else
        {
          cout << "\033[104Computer shoot in " << position << "\033[104 and missed " << "\033[0m" << endl;
        }
        
      }
      while (true);
  }

  Position Program::ParsePosition(string input)
    {
      char cColumn = toupper(input.at(0));
      char cRow = input.at(1);

	  int nColumn = (cColumn - 'A');
      Letters lColumn = (Letters)nColumn;

      int nRow = cRow - '0';

	  Position outPosition;
	  outPosition.Column = lColumn;
	  outPosition.Row = nRow;
	  return outPosition;
    }

    Position Program::GetRandomPosition()
    {
      const int size = fieldSize;
      srand((unsigned int) time(NULL));
      Letters lColumn = (Letters)(rand() % size);
      int nRow = (rand() % size);

      Position position(lColumn, nRow);
      return position;
    }

    bool Program::checkInField(Position shot) {
       cout << "Entering check in field \n";
       cout << shot.Column << "    " << shot.Row << endl;
        if (shot.Column < 0 || shot.Column > (fieldSize - 1) || shot.Row < 1 || shot.Row > fieldSize) {
            cout << "\033[101mPosition was not in Battlefield range"  << "\033[0m" << endl;
            return false;
        }

        cout << "returning false \n";
        return true;
    }


    void Program::InitializeGame()
    {
      InitializeMyFleet();

      InitializeEnemyFleet(enemyFleet);
    }

	void Program::InitializeMyFleet()
	{
		myFleet = GameController::GameController::InitializeShips();


		cout << "Please position your fleet (Game board has size from A to " << (char)(fieldSize + 'A' - 1) << " and 1 to " << fieldSize << ") :" << endl;
		for_each(myFleet.begin(), myFleet.end(), [](Ship &ship)
		{
			cout << endl;
			cout << "Please enter the positions for the " << ship.Name << " (size: " << ship.Size << ")" << endl;
			for (int i = 1; i <= ship.Size; i++)
			{
        cout << "Enter position " << i << " of " << ship.Size << "\n";
				string input;
				getline(cin, input);
				Position inputPosition = ParsePosition(input);

                if (!ship.AddPosition(inputPosition)) {
                    cout << "\033[101mPosition is invalid. Please choose a new one" << "\033[0m" << endl;
                    i--;
                }
			}
		});
	}

	void Program::InitializeEnemyFleet(list<Ship>& Fleet)
	{
		Fleet = GameController::GameController::InitializeShips();

		for_each(Fleet.begin(), Fleet.end(), [](Ship& ship)
		{
			if (ship.Name == "Aircraft Carrier")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 4));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 6));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 7));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 8));
			}
			if (ship.Name == "Battleship")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 6));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 7));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 8));
			}
			if (ship.Name == "Submarine")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 3));
			}
			if (ship.Name == "Destroyer")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::F, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::G, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::H, 8));
			}
			if (ship.Name == "Patrol Boat")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 6));
			}
		});
	}
  }
}