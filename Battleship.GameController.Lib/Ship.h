#pragma once

#include <string>
#include <list>

#include "Position.h"

namespace Battleship
{
  namespace GameController
  {
	namespace Contracts
	{
	  class Ship
	  {

	  public:
		Ship();
		Ship(std::string Name, int Size);
		~Ship();

	  public:

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		std::string Name;

		/// <summary>
		/// Gets or sets the positions.
		/// </summary>
		std::list<Position> Positions;

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		int Size;

		bool isSunk{false};

		/// <summary>
		/// The add position.
		/// </summary>
		/// <param name="input">
		/// The input.
		/// </param>
		/// <returns>
		/// True if possible to insert, else if not
		/// </returns>
		bool AddPosition(std::string input);
		bool AddPosition(const Position &input);
	  
	  private:
		/// <summary>
		/// Verify if Position is valid or not .
		/// </summary>
		/// <param name="input">
		/// The input.
		/// </param>
		/// <returns>
		/// True if possible to insert, else if not
		/// </returns>
		bool VerifyInsertPosition(const Position& inputPosition);
		bool isSunken();
	  };
	}
  }
}

