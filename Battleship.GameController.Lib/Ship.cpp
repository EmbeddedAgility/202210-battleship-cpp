#include "Ship.h"
#include <iostream>
#include <algorithm>
using namespace std;

namespace Battleship
{
  namespace GameController
  {
	namespace Contracts
	{
	  Ship::Ship()
	  {
	  }

	  Ship::Ship(std::string Name, int Size) : Name(Name), Size(Size)
	  {
	  }

	  Ship::~Ship()
	  {
	  }

	  bool Ship::AddPosition(string input)
	  {
		char cColumn = toupper(input.at(0));
		char cRow = input.at(1);  
		
		Letters lColumn = (Letters) (cColumn - 'A');
		int nRow = cRow - '0';
		
		Position position(lColumn, nRow, PositionType::SHIP);
		if(VerifyInsertPosition(position)){
			Positions.insert(Positions.begin(), position);
			return true;
		}
		return false;
	  }

	  bool Ship::AddPosition(const Position &inputPosition)
	  {
		  if(VerifyInsertPosition(inputPosition))
		  { 
			  Positions.insert(Positions.begin(), inputPosition);
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	  }

	  bool Ship::VerifyInsertPosition(const Position &inputPosition)
	  {
		  //validate if it's the first position
		  if (Positions.size() == 0) {
			  return true;
		  }
		  else {
				bool adjacentValid = false;
				bool actualPos = true;
			  // Verify if it have an adjacent position
			  for (Position adj : Positions) {
				  if ((adj.Column + 1 == inputPosition.Column && adj.Row == inputPosition.Row && adj.positionType != PositionType::SHIP) ||
					  (adj.Column - 1 == inputPosition.Column && adj.Row == inputPosition.Row && adj.positionType != PositionType::SHIP) ||
					  (adj.Column == inputPosition.Column && adj.Row + 1 == inputPosition.Row && adj.positionType != PositionType::SHIP) ||
					  (adj.Column == inputPosition.Column && adj.Row - 1 == inputPosition.Row && adj.positionType != PositionType::SHIP)) {
					  adjacentValid = true;
				  }
				  if(adj.Column == inputPosition.Column && adj.Row == inputPosition.Row)
				  	{
						actualPos = false;
					}
			  }
			  if(adjacentValid && actualPos)
			  {
				return true;
			  }
		  }
		  return false;
	  }




	  bool Ship::isSunken()
	  {
		for(auto it = Positions.begin(); it != Positions.end(); it++)
		{
			if(it->positionType == PositionType::SHIP){
				return false;
			}
		}
		isSunk = true;
		cout << "\033[101mThe ship is sunken!" << endl;
		cout << endl;
      	cout << "\033[0m";
		return true;
	  }
	}
  }
}


