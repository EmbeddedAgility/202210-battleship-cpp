#include "Position.h"

namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
      Position::Position(Letters column, int row, PositionType type) 
      : Column(column)
      , Row(row)
      , positionType(type) 
      {
      }

      Position::Position(const Position &init) 
      : Column(init.Column)
      , Row(init.Row)
      , positionType(init.positionType)
      {
      }

      Position::~Position()
      {
      }

      Position &Position::operator=(const Position &rhs)
      {
        Column = rhs.Column;
        Row = rhs.Row;
        positionType = rhs.positionType;

        return *this;
      }

      bool Position::operator==(const Position &rhs) const
      {
        return (Column == rhs.Column) && (Row == rhs.Row);
      }

      void Position::setType(PositionType& type)
      {
        positionType = type;
      }
    }
  }
}

