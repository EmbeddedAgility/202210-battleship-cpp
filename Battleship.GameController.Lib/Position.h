#pragma once

#include "Letters.h"
#include "PositionType.h"

namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
      class Position
      {
      public:
        Position(Letters column = Letters::A, int row = 0, PositionType type = PositionType::WATER);
        Position(const Position &init);
        ~Position();
      
      public:
        Letters Column;
        int Row;
        PositionType positionType;

      public:
        Position &operator=(const Position &rhs);
        bool operator==(const Position &rhs) const;
        void setType(PositionType &type);
      };
    }
  }
}


