#pragma once

namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
        enum PositionType
        {
          WATER = 0,
          SHIP,
          HIT
        };
    }
  }
}